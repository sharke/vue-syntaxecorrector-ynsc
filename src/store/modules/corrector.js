import axios from "axios";

const state = {
  inputs: "",
  corrections: [],
};

// languages : [ enUS  , frFR]
const apiCall = (inputs, language = "frFR") => ({
  method: "POST",
  url: "https://jspell-checker.p.rapidapi.com/check",
  headers: {
    "content-type": "application/json",
    "X-RapidAPI-Key": "e259ce5e98mshf3a8c46f66d051dp1d3c7fjsn73959730b922",
    "X-RapidAPI-Host": "jspell-checker.p.rapidapi.com",
  },
  data: `{"language":"${language}","fieldvalues":"${inputs}","config":{"forceUpperCase":false,"ignoreIrregularCaps":false,"ignoreFirstCaps":true,"ignoreNumbers":true,"ignoreUpper":false,"ignoreDouble":false,"ignoreWordsWithNumbers":true}}`,
});

// GETTERS
const getters = {
  allErrors(state) {
    return state.corrections;
  },
  getInputs(state) {
    return state.inputs;
  },
};

// ACTIONS
const actions = {
  async fetchCorrections({ commit }) {
    try {
      console.log("fetchCorrections >> test ");
      let AllData = await axios.request(apiCall(state.inputs));
      let { elements } = AllData.data;

      if (AllData.data.elements) {
        console.log("AllData >>  ", elements[0].errors);
        commit("setCorrections", elements[0].errors);
      }
    } catch (e) {
      console.error("error fatch data ");
      console.error(e);

      throw e;
    }
  },
  async setInputs({ commit }, input) {
    commit("setInputs", input);
  },
};

// MUTATIONS
const mutations = {
  setCorrections(state, payload) {
    state.corrections = payload;
  },
  setInputs(state, payload) {
    state.inputs = payload;
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
